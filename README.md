# Giftwrap
GiftWrap turns any website into a single page application using React.
It wraps the original site pages, gives you a persistent memory space and all the ES6 goodies out of the box including ES6 modules, React Components, Browserify/Watchify build system, and Less CSS.

The use case for Giftwrap might not be obvious.
Think about starting work on a legacy web application that's been built using a multi-page model, but it really would work better as a SPA.
Refactoring the entire front end in one shot might not be feasible.
Instead, you can giftwrap your old app and refactor the app one component or page at a time.
Your old server rendered pages can work seamlessly with new client rendered pages until the refactor is complete and you're left with a clean React application.

## Check out the demo:
- `git clone git@github.com:lattapartners/giftwrap.git`
- `npm install`
- `gulp`
- `node demoServer.js`
- Open browser to http://localhost:3000/

You'll notice navigation feels natural with push state, forward button, back button, and refresh support.
Visually the pages look and scroll exactly like the wrapped application.
The only difference is that the wrapped application's url's have a prefix (/app by default).

## Giftwrapping your existing application

Todo: make this setup easier.

- Copy the following files / folders to a directory above where your existing site is served from
  * /app
  * /config
  * package.json
  * gulpfile.js
- Update the publicWebRoot value in /config/appConfig.js file to reflect the directory your current site is served from. If you change this value, also update the gulp.js file.
- Update your web server
  * Add server routes similar to those found in demoServer.js to your web server
  ```
  //url where the single page app will be served from
  app.get('/app', function (req, res) {
    res.render('app.html');
  })
  app.get('/app/*', function (req, res) {
    res.render('app.html');
  })
  ```
-
