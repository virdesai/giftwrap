import React from 'react';
import Router from 'react-router';
import IframeContainer from 'iframeContainer';
import JsRenderDemo from 'jsRenderDemo';
import _ from 'lodash';
import appGlobals from 'appGlobals';

var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var RouteHandler = Router.RouteHandler;
var DefaultRoute = Router.DefaultRoute;

//Top level application component
class _App {

  getInitialState() {
    return {
      loadReady: false
    }
  }

  componentWillMount() {
    //Do application prep like loading data from async sources before rendering
    //...

    //Then set state to ready to render application
    this.setState({
      loadReady: true
    })
  }

  render() {
    //Add any layout markup here.

    if(this.state.loadReady)
      return <RouteHandler/>
    else
      return <div>Loading...</div>
  }
}
var App = React.createClass(_App.prototype);

//Router:
// All routes will render in an iframe that mirrors the url
var routes = (
  <Route name="app" path="/app" handler={App} >
    <DefaultRoute handler={IframeContainer} />
    <Route name="jsRender" path="rendered-in-react" handler={JsRenderDemo} />
    <Route name="iframe" path=":pagePath" handler={IframeContainer} />
  </Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
  React.render(<Handler/>, document.getElementById('content'));
});

window.appGlobals = appGlobals;
