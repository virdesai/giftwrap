import demoModule from 'demoModule';
import React from 'react';

//Export modules to the old multi-page site
var appGlobals = {
  React: React,
  demoModule: demoModule
}

export default appGlobals;