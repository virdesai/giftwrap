//Demo for an ES6 class module

var bornDate = new Date().getTime();

class DemoClass {
  message() {
    return 'Hi from ES6 Module!'
  }

  get age() {
    var now = new Date().getTime();
    return parseInt((now-bornDate)/1000);
  }
}

var instance = new DemoClass();

export default instance;