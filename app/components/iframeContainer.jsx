import React from 'react';
import config from 'appConfig';
import Router from 'react-router';
import qs from 'qs';

function removeTrailingSlash(str){
  if(str.lastIndexOf('/') === str.length-1)
    str = str.slice(0,str.length-1);
  return str;
}

function translateUrlForIframe(path) {
  //Replace paths
  return path.replace(removeTrailingSlash(config.appUrlRoot), '/').replace('//','/');
}

function translateUrlForPushstate(path){
  var root = removeTrailingSlash(config.appUrlRoot);
  if(path.startsWith(root))
    root = '';
  return root + path
}


var _IframeContainer = {
  mixins : [ Router.Navigation, Router.State ],
  componentDidMount :  function() {
    console.log('component did mount')
    var iframe = this.refs.iframe.getDOMNode();

    var iframeOnload = ()=>{
      var location = iframe.contentWindow.location;
      var iframeUrl = removeTrailingSlash(location.pathname);
      var iframeHash = location.hash || '';
      var iframeQuery = location.search || '';

      var newUrl = translateUrlForPushstate(iframeUrl + iframeQuery + iframeHash);
      this.replaceWith(newUrl);
    };

    iframe.onload = iframeOnload;
  },

  render : function() {
      var querystring = qs.stringify(this.getQuery());
      querystring = querystring ? ('?'+ querystring) : '';
      var src = '/' + (this.getParams().pagePath || '') + querystring + window.location.hash;
      src = translateUrlForIframe(src);
      return <iframe id="mainframe" ref="iframe" src={src}></iframe>;
  }
}

export default React.createClass(_IframeContainer);