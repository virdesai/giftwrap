import React from 'react';
import Router from 'react-router';
const Link = Router.Link;

var _JsRenderSample = {

  render: function() {
    var style = {padding:'10px'}
    return <div style={style}>
            <h1>Rendered with React!</h1>
            <ul>
              <li><Link to="app">Home</Link></li>
              <li><Link to="iframe" params={{pagePath: "page2.html"}}>Scrolling Page</Link></li>
              <li><Link to="jsRender">Client side rendered View</Link></li>
              <li><Link to="iframe" params={{pagePath: "page1.html"}} query={{message: "hello"}}>Works with Querystrings</Link></li>
              <li><Link to="iframe" params={{pagePath: "page2.html"}}>Handles hashes like a champ</Link></li>
            </ul>
          </div>
  }
}

export default React.createClass(_JsRenderSample);
