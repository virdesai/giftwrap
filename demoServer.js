var express = require('express')
var app = express()


//Enable rendering html file for root of app
app.set('views', __dirname + '/www');
app.engine('html', require('ejs').renderFile);



//Create these two routes in your production server
app.get('/app', function (req, res) {
  res.render('app.html');
})

app.get('/app/*', function (req, res) {
  res.render('app.html');
})

//Static file server
app.use(express.static(__dirname + '/www'));

var server = app.listen(5000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('Example app listening at http://%s:%s', host, port)
})