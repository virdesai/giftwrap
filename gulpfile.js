//Utils
var gulp = require('gulp');
var fs = require('fs');
var vm = require('vm');
var gutil = require('gulp-util');
var config = require('./config/appConfig');
var watch = require('gulp-watch');

//JS build
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var watchify = require('watchify');
var es6ify = require('es6ify');
var reactify = require('reactify');
var source = require('vinyl-source-stream');

//CSS
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');

/** File paths */
var paths = {
    dist: '.' + config.publicWebRoot,
    entries: ['./app/app.jsx'],
    modules: ['node_modules', './app', './app/react', './app/modules','./app/components', './config'],
    vendorScripts: [
        'node_modules/es6ify/node_modules/traceur/bin/traceur-runtime.js',
        'app/vendor/**/*.js'],
    requireFiles:  './node_modules/react/react.js',
    less: './app/less/*.less'
}
paths.scripts = paths.dist + 'scripts/';
paths.css = paths.dist + '/styles/';

//Vendor scripts build
gulp.task('vendor', function () {
    return gulp.src(paths.vendorScripts)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts));
});

//Less CSS build
gulp.task('styles', function() {
    gulp.src(paths.less)
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(gulp.dest(paths.css));
})

//Watchify / Browserify
var watchifyOptions = {
    entries: paths.entries,
    extensions: ['.js', '.jsx'],
    paths: paths.modules
}

function compileScripts() {
    es6ify.traceurOverrides = {experimental: true};
    bundler = watchify(watchifyOptions);
    bundler.require(paths.requireFiles);
    bundler.transform(reactify);
    // bundler.transform(es6ify.configure(/.jsx|app\/compnents|app\/scripts\//));
    bundler.transform(es6ify.configure(/.jsx|app\/modules/));

    var rebundle = function () {
        var stream = bundler.bundle({ debug: true });

        stream.on('error', function (err) { console.error(err) });
        stream = stream.pipe(source(watchifyOptions.entries[0]));
        stream.pipe(rename('app.js'));
        stream.pipe(gulp.dest(paths.scripts));
        gutil.log('finished building \'app.js\'');
    };

    bundler.on('update', rebundle);
    return rebundle();
}

gulp.task('default', ['vendor', 'styles'], function () {
    compileScripts();
    watch(paths.css, ['styles']);
});
